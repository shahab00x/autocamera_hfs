classdef hfs_random_task
    properties
        a_e = 'ABCDE';
        f_j = 'FGHIJ';
        warmup_count = 1;
        pattern_count = 3;
        number_of_instructions_per_row = 3;
        camera_control_methods = {'Clutch and Move', 'Autocamera', 'Joystick'};
    end
    
    methods
        function self = hfs_random_task()
        
            
        end
        function r = random_hole(self, numbers_range, letters) 
            n = randi(numbers_range);
            l = letters( randi([1,numel(letters)]) );
            
            r = [l, num2str(n)];
        end
        
        function r = generate_new_pattern(self, seed)
           top = {};
           mid1 = {};
           mid2 = {};
           bot = {};
           if nargin <2
               seed = 1;
           end
           rng(seed);
           for i = floor(linspace(5,45, self.number_of_instructions_per_row))
              h1 = self.random_hole([i,i+10], self.a_e); 
              top{end+1} = h1;
              h2 = self.random_hole([i,i+10], self.f_j); 
              mid1{end+1} = h2;
           end
           
           
           for i = floor(linspace(5,45, self.number_of_instructions_per_row))
              h3 = self.random_hole([i,i+10], self.a_e); 
              mid2{end+1} = h3;
              h4 = self.random_hole([i,i+10], self.f_j); 
              bot{end+1} = h4;
           end
           
           r = struct();
           r.top = top;
           r.mid1 = mid1;
           r.mid2 = mid2;
           r.bot = bot;
        end
        
        function r = generate_a_subject_case(self, patterns)
            % Create a random permutation of all the pairs of camera
            % control methods and breadboard patterns            
            all = {};
            for i=self.camera_control_methods
                for j=patterns
                    all{end+1}= struct('method', i, 'pattern', j);
                end
            end
            
            r = all(randperm( numel(patterns) * numel(self.camera_control_methods)));
        end
        
        function r = export_warmup_tasks(self,subject_number)
            images = {};
            outputs = {};
            for i=1:self.warmup_count
               images{i} = imread( sprintf('pattern%i.bmp', i)); 
            end
            
            test_cases = self.generate_a_subject_case( 1:self.warmup_count);
            file_name = sprintf('test_cases/subject%i', subject_number);
            mkdir( file_name);
            blank = ones(size(images{1}))*255; % a blank white page
            
            for num=1:numel(test_cases)
                pattern_num = test_cases{num}.pattern;
                text = sprintf('Subject Number: %i\nTask Number: %i\nMethod: %s\nPattern Number: %i\n',subject_number, num, test_cases{num}.method, pattern_num);
                outputs{num} = AddTextToImage(images{pattern_num}, text, [200, 600], [0,0,0], 'Ariel', 180); 
                backside = AddTextToImage(blank, text, [200, 600], [0,0,0], 'Ariel', 180); 
                imwrite(outputs{num}, sprintf('%s/%i.bmp', file_name, num));
                imwrite(backside, sprintf('%s/%i_backside.bmp', file_name, num));
            end
            r = outputs;
            
            
        end
        
        function r = export_random_test_case(self,subject_number)
            images = {};
            outputs = {};
            for i=(self.warmup_count+1):(self.warmup_count + self.pattern_count)
               images{i} = imread( sprintf('pattern%i.bmp', i)); 
            end
            
            test_cases = self.generate_a_subject_case((self.warmup_count+1):(self.warmup_count + self.pattern_count) );
            file_name = sprintf('test_cases/subject%i', subject_number);
            mkdir( file_name);
            blank = ones(size(images{end}))*255; % a blank white page
            
            for num=1:numel(test_cases)
                pattern_num = test_cases{num}.pattern;
                task_number = num + self.warmup_count* numel(self.camera_control_methods)
                text = sprintf('Subject Number: %i\nTask Number: %i\nMethod: %s\nPattern Number: %i\n',subject_number, task_number, test_cases{num}.method, pattern_num);
                outputs{num} = AddTextToImage(images{pattern_num}, text, [200, 600], [0,0,0], 'Ariel', 180); 
                backside = AddTextToImage(blank, text, [200, 600], [0,0,0], 'Ariel', 180); 
                imwrite(outputs{num}, sprintf('%s/%i.bmp', file_name, task_number));
                imwrite(backside, sprintf('%s/%i_backside.bmp', file_name, task_number));
            end
            r = outputs;
            
            
        end
    end
end